package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//This file serves as our index/entry point


// @SpringBootApplication this is called as "Annotation" Mark
// represented by the @ symbol
// This is a shortcut or method we manually create or describe our purpose or configure a framework
// Annotation are used to provide supplemental information about the program

@SpringBootApplication
//Tells the sringboot that this will handle
@RestController
public class Wdc044Application {
	//This method starts the whole Springboot Framework
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	// how to run our application?
	// ./mvnw spring-boot:run

	// Mapping HTTP Get Request
	@GetMapping("/hello")

	//Access query parameter
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")

	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet){
		return String.format("Good Evening, %s! Welcome to Batch 293!", greet);
	}


	//Activity
	@GetMapping("/hi")

	public String hi(@RequestParam(value = "intro", defaultValue = "user") String intro){
		return String.format("Hi %s", intro);
	}

	@GetMapping("/nameAge")

	public String nameAge(@RequestParam(value = "age", defaultValue = "1") String age){
		return String.format("Hi Juan! Your age is %s", age);
	}

	@GetMapping("/home")

	public String all(@RequestParam(value = "sIntro", defaultValue = "user") String sIntro,
			@RequestParam(value = "age", defaultValue = "1") String age) {
		return String.format("Hi %s! Your age is %s", sIntro, age);
	}

	// params: http://localhost:8080/home?sIntro=Mac&age=23






}
