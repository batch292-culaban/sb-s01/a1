package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {

    // create a post method
    void createPost(String stringToken, Post post);

    // getting all post
    Iterable<Post> getPosts();

    //Edit a user's post method
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //Delete a user's post method
    ResponseEntity deletePost(Long id, String stringToken);

}
