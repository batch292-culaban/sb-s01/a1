package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import io.jsonwebtoken.Jwt;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Object getUser(String stringToken){
       User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
       user.setPassword("");
       return user;
    }


    public Object getMyPosts(String stringToken){
        User userPosts = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        userPosts.setPassword("");
        return userPosts.getPosts();
    }
}
